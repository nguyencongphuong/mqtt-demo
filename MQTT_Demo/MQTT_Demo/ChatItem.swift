//
//  ChatItem.swift
//  MQTT_Demo
//
//  Created by Anh Dang Nhan on 8/11/16.
//  Copyright © 2016 AnhDN. All rights reserved.
//

import Foundation
import ObjectMapper


public class ChatItem : Mappable{
    
    public var content:String!
    public var user_name:String!
    
    init(content : String, user_name : String){
        self.content = content
        self.user_name = user_name
    }
    
    
    required public init?(_ map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        content    <- map["content"]
        user_name         <- map["username"]
    }
}