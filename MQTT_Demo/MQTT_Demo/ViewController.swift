//
//  ViewController.swift
//  MQTT_Demo
//
//  Created by Admin on 8/9/16.
//  Copyright © 2016 AnhDN. All rights reserved.
//

import UIKit
import NoChat
import CocoaMQTT
import Foundation
//import SwiftyJSON

class ViewController: UIViewController{

    var mqtt: CocoaMQTT!
    
    let name = "phuong"
    
    @IBOutlet weak var mess1: UILabel!
    
    @IBOutlet weak var mess2: UILabel!
    
    @IBOutlet weak var mess3: UILabel!
    
    @IBOutlet weak var mess4: UILabel!
    
    @IBOutlet weak var lblNotify: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        mqttSetting()
        mqtt.connect()
        //sendMessage()
        // Do any additional setup after loading the view, typically from a nib.
        
        let notificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)

    }
    
    
    
    
    func mqttSetting() {
        //let clientIdPid = "CocoaMQTT-\(UIDevice.currentDevice().identifierForVendor!.UUIDString)"
        mqtt = CocoaMQTT(clientId: name, host: "123.31.11.237", port: 1883)
        //mqtts
        //let mqtt = CocoaMQTT(clientId: clientIdPid, host: "localhost", port: 8883)
        //mqtt.secureMQTT = true
        if let mqtt = mqtt {
            mqtt.username = "test"
            mqtt.password = "public"
            mqtt.willMessage = CocoaMQTTWill(topic: "/will", message: "dieout")
            mqtt.keepAlive = 90
            mqtt.delegate = self
        }
        
        
    }
    
    func receiverChat(){
        
    }
    
    var messageItem:ChatItem!
    func reciverMess(mess:String,topic:String){
        
        
        let nsString =  mess as NSString
        let myNSData = nsString.dataUsingEncoding(NSUTF8StringEncoding)!
        
        do{
//            let myNSData = try NSJSONSerialization.dataWithJSONObject(text, options: NSJSONWritingOptions.PrettyPrinted)
            let json = try NSJSONSerialization.JSONObjectWithData(myNSData, options: .MutableContainers) as! NSDictionary
            
            let username = json["username"] as? String
            let content = json["content"] as? String
            
            messageItem = ChatItem(content: content!, user_name: username!)
            
            if(topic == "notify"){
                
                lblNotify.text = username!  + ": " + content!
            }else{
                
                switch messageItem.user_name {
                case "phuong":
                    mess1.text = messageItem.content
                case "nhananh":
                    mess2.text = messageItem.content
                case "thang":
                    mess3.text = messageItem.content
                default:
                    break
                }
            }
            
        }catch{
            
        }
        
    }
    

    
    var secondViewController: ChatView!
    @IBAction func chat1(sender: AnyObject) {
        if(secondViewController == nil || secondViewController.sender != "phuong")
        {
          secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChatView") as! ChatView
          secondViewController.sender = "phuong"
          secondViewController.mqtt = mqtt
        }
         self.navigationController!.pushViewController(secondViewController, animated: true)
    }
    
    
    @IBAction func chat2(sender: AnyObject) {
        if(secondViewController == nil || secondViewController.sender != "nhananh")
        {
         secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChatView") as! ChatView
         secondViewController.sender = "nhananh"
         secondViewController.mqtt = mqtt
        }
         self.navigationController!.pushViewController(secondViewController, animated: true)

    }
    
    @IBAction func chat3(sender: AnyObject) {
        if(secondViewController == nil || secondViewController.sender != "thang")
        {
          secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChatView") as! ChatView
          secondViewController.sender = "thang"
          secondViewController.mqtt = mqtt
        }
         self.navigationController!.pushViewController(secondViewController, animated: true)

    }
    
//    @IBAction func chat4(sender: AnyObject) {
//        
//        let secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChatView") as! ChatView
//        
//        self.navigationController!.pushViewController(secondViewController, animated: true)
//        
//        secondViewController.sender = "tuan"
//    }
//    
    
    
    @IBAction func sendLog(sender: AnyObject) {
        
        let notification = UILocalNotification()
        notification.fireDate = NSDate(timeIntervalSinceNow: 5)
        notification.alertBody = "Hey you! Yeah you! Swipe to unlock!"
        notification.alertAction = "be awesome!"
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.userInfo = ["CustomField1": "w00t"]
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([ .Hour, .Minute, .Second], fromDate: date)
        
        let data = ["username":name,"content" : "Log at \(components.hour) : \(components.minute) : \(components.second)"]
        
        mqtt!.publish("tracklog", withString: data.description, qos: .QOS1)
        
    }
    
}

extension ViewController: CocoaMQTTDelegate{
    
    func mqtt(mqtt: CocoaMQTT, didConnect host: String, port: Int) {
        print("didConnect \(host):\(port)")
    }
    
    func mqtt(mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        print("didConnectAck \(ack.rawValue)")
        if ack == .ACCEPT {
            mqtt.subscribe("chat/+/\(name)", qos: CocoaMQTTQOS.QOS1)
            mqtt.subscribe("notify", qos: CocoaMQTTQOS.QOS1)
            mqtt.subscribe("tracklog", qos: CocoaMQTTQOS.QOS1)
            
            mqtt.ping()
            
            
        }
        
    }
    
    func mqtt(mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
        print("didPublishMessage with message: \(message.string)")
    }
    
    func mqtt(mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        print("didPublishAck with id: \(id)")
    }
    
    func mqtt(mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
        print("didReceivedMessage: \(message.string) with id \(id)")
        
        reciverMess(message.string!, topic: message.topic)
        
        
        if(secondViewController != nil && message.topic != "notify")
        {
            secondViewController.receivedMessage(message.string!)
        }
        
        print(message.topic)
    }
    
    func mqtt(mqtt: CocoaMQTT, didSubscribeTopic topic: String) {
        print("didSubscribeTopic to \(topic)")
    }
    
    func mqtt(mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
        print("didUnsubscribeTopic to \(topic)")
    }
    
    func mqttDidPing(mqtt: CocoaMQTT) {
        print("didPing")
    }
    
    func mqttDidReceivePong(mqtt: CocoaMQTT) {
        _console("didReceivePong")
    }
    
    func mqttDidDisconnect(mqtt: CocoaMQTT, withError err: NSError?) {
        _console("mqttDidDisconnect")
    }
    
    func _console(info: String) {
        print("Delegate: \(info)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}

