//
//  ChatViewController.swift
//  MQTT_Demo
//
//  Created by Admin on 8/11/16.
//  Copyright © 2016 AnhDN. All rights reserved.
//

import UIKit
import CocoaMQTT
import ObjectMapper

class ChatView: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var editView: UIView!
    @IBOutlet weak var editMess: UITextField!
    @IBOutlet var Bottom: NSLayoutConstraint!
    var mqtt:CocoaMQTT!
    var listItem = [ChatItem]()
    let name = "phuong"
    var sender:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        editMess.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatView.keyboardWasShown(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatView.keyboardwillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func keyboardWasShown(notification: NSNotification) {
    
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            
            self.Bottom.constant = keyboardFrame.size.height + 20
        })
    }
    
    func keyboardwillHide(notification: NSNotification) {
        
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            
            self.Bottom.constant = 20
        })
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        editMess.resignFirstResponder()
        return true
    }
    
    @IBAction func sendMess(sender: AnyObject) {
        sendMessage()
    }
    
    
    func sendMessage() {
        let item = ChatItem(content: editMess.text!, user_name: name)
        let jsonMess = Mapper().toJSONString(item, prettyPrint: true)
        
        mqtt!.publish("chat/\(name)/\(sender)", withString: jsonMess!, qos: .QOS1)
        
        
        listItem.append(item)
        tableView.reloadData()
        
        editMess.text = ""
        scrollToBottom()
        
    }
    
   internal func receivedMessage(message:String){
    
        let chatItem = Mapper<ChatItem>().map(message)
    
        if(chatItem?.user_name == sender)
        {
            listItem.append(chatItem!)
            tableView.reloadData()
            scrollToBottom()
        }


    }
    
    func scrollToBottom(){
        let count = listItem.count
        let indexPath = NSIndexPath(forRow: count - 1, inSection: 0)
        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItem.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let message =  listItem[indexPath.row]
        
        if(message.user_name == name){
            let cell = tableView.dequeueReusableCellWithIdentifier("right", forIndexPath: indexPath) as! RightCell
            cell.messenger.text =  listItem[indexPath.row].content
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("left", forIndexPath: indexPath) as! LeftCell
            cell.messenger.text = listItem[indexPath.row].content
            return cell
        }
    }
    
    
    
    
    
    
//    
//    func mqtt(mqtt: CocoaMQTT, didConnect host: String, port: Int) {
//        print("didConnect \(host):\(port)")
//    }
//    
//    func mqtt(mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
//        print("didConnectAck \(ack.rawValue)")
//        if ack == .ACCEPT {
//            mqtt.subscribe("chat/+/\(name)", qos: CocoaMQTTQOS.QOS1)
//            mqtt.ping()
//            
//        }
//        
//    }
//    
//    func mqtt(mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
//        print("didPublishMessage with message: \(message.string)")
//    }
//    
//    func mqtt(mqtt: CocoaMQTT, didPublishAck id: UInt16) {
//        print("didPublishAck with id: \(id)")
//    }
//    
//    func mqtt(mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
//        print("didReceivedMessage: \(message.string) with id \(id)")
//        receivedMessage(message.string!)
//        print(message.topic)
//    }
//    
//    func mqtt(mqtt: CocoaMQTT, didSubscribeTopic topic: String) {
//        print("didSubscribeTopic to \(topic)")
//    }
//    
//    func mqtt(mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
//        print("didUnsubscribeTopic to \(topic)")
//    }
//    
//    func mqttDidPing(mqtt: CocoaMQTT) {
//        print("didPing")
//    }
//    
//    func mqttDidReceivePong(mqtt: CocoaMQTT) {
//        _console("didReceivePong")
//    }
//    
//    func mqttDidDisconnect(mqtt: CocoaMQTT, withError err: NSError?) {
//        _console("mqttDidDisconnect")
//    }
//    
//    func _console(info: String) {
//        print("Delegate: \(info)")
//    }
    

}
