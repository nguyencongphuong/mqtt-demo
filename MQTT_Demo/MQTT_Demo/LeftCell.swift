//
//  LeftCell.swift
//  MQTT_Demo
//
//  Created by Anh Dang Nhan on 8/11/16.
//  Copyright © 2016 AnhDN. All rights reserved.
//

import UIKit

class LeftCell: UITableViewCell {

    @IBOutlet weak var messenger: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
